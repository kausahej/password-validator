/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week8;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sahej
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testValidatePasswordLengthRegular() {
        System.out.println("validate Password Length Regular");
        String password = "abcdefghij";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The length does not meet the requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
        
    @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validate Password Length exception");
        String password = "";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The length of the password does not meet the requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
      @Test
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validate Password Length boundary In");
        String password = "abcdefgh";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The length of the password does not meet the requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validate Password Length boundary out");
        String password = "abcdefg";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The length of the password does not meet the requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidatePasswordSpecialCharactersRegualr() {
        System.out.println("Validate Password Special Characters Regular");
        String password = "@$+!#?^&";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not meet special Characters requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    @Test
    public void testValidatePasswordSpecialCharactersException() {
        System.out.println("Validate Password Special Characters Exception");
        String password = "12345678";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not meet special Characters requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidatePasswordSpecialCharactersBoundaryIn() {
        System.out.println("Validate Password Special Characters BoundaryIn ");
        String password = "%gh()}{[]";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not meet special Characters requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
 }
    
     @Test
    public void testValidatePasswordSpecialCharactersBoundaryOut() {
        System.out.println("Validate Password Special Characters BoundaryOut");
        String password = "%gh()";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not meet special Characters requirement",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordOneUppercaseCharacterRegular() {
        System.out.println("Validate Password with One Uppercase Character ");
        String password = "Abcdeghji";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one uppercase character ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    @Test
    public void testValidatePasswordOneUppercaseCharacterException () {
        System.out.println("Validate Password with One Uppercase Character ");
        String password = "abcdeghjiu";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one uppercase character ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testValidatePasswordOneUppercaseCharacterBoundaryIn () {
        System.out.println("Validate Password with One Uppercase Character ");
        String password = "Abcdefgh";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one uppercase character ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    @Test
    public void testValidatePasswordOneUppercaseCharacterBoundaryOut () {
        System.out.println("Validate Password with One Uppercase Character ");
        String password = "abcdeghio";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one uppercase character ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    @Test
    public void testValidatePasswordOneDigitRegular () {
        System.out.println("Validate Password with atleast one digit ");
        String password = "abcdefgh4";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one digit ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
     @Test
    public void testValidatePasswordOneDigitException () {
        System.out.println("Validate Password with atleast one digit ");
        String password = "12345678";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one digit ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testValidatePasswordOneDigitBoundaryIn () {
        System.out.println("Validate Password with atleast one digit ");
        String password = "abcdefgh8";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one digit ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testValidatePasswordOneDigitBoundaryOut () {
        System.out.println("Validate Password with atleast one digit ");
        String password = "12374677667";
        PasswordValidator instance = new PasswordValidator();
        boolean expResult = false;
        boolean result = instance.validate(password);
        assertEquals("The password does not have atleast one digit ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
}
